# where-is-iss-now
Aplikacja umożliwia pobranie danych z API - pozycja ISS, następnie renderuje pozycję na mapie google. Jednocześnie osobny komponent wyświetla dane (pozycja, prędkość, wysokość). 
W każdej chwili można sprawdzić nową pozycję ISS oraz włączyć automatyczne odświeżanie pozycji (aktualnie co 10s). 

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Run your unit tests
Niestety nie zdążyłem napisać testów

```
npm run test:unit
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
